package com.ywwxhz.model;

import java.util.ArrayList;
import java.util.List;

public class Category {
    private String Type;
    private List<AppInfo> apps;

    public Category(String Type) {
        this.Type = Type;
        setApps(new ArrayList<AppInfo>());
    }

    public List<AppInfo> getAppInfos() {
        return getApps();
    }

    public void add(AppInfo appInfo) {
        getApps().add(appInfo);
    }

    public void remove(int pos) {
        getApps().remove(pos);
    }

    public AppInfo getAppInfo(int pos) {
        return getApps().get(pos);
    }

    public int getSize() {
        return getApps().size();
    }

    public void clear() {
        getApps().clear();
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public List<AppInfo> getApps() {
        return apps;
    }

    public void setApps(List<AppInfo> apps) {
        this.apps = apps;
    }

}
