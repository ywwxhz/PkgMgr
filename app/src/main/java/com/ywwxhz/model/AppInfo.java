package com.ywwxhz.model;

import android.graphics.drawable.Drawable;

//Model类 ，用来存储应用程序信息
public class AppInfo {

    private String Label;    //应用程序标签
    private Drawable appIcon;  //应用程序图像
    private String pkgName;    //应用程序所对应的包名
    private int versionCode;
    private String version;
    private String sourceDir;
    private int type;
    private boolean backup;
    private boolean select;

    public AppInfo() {
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String appName) {
        this.Label = appName;
    }

    public Drawable getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(Drawable appIcon) {
        this.appIcon = appIcon;
    }

    public String getPkgName() {
        return pkgName;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSourceDir() {
        return sourceDir;
    }

    public void setSourceDir(String sourceDir) {
        this.sourceDir = sourceDir;
    }

    public boolean isBackup() {
        return backup;
    }

    public void setBackup(boolean backup) {
        this.backup = backup;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}

