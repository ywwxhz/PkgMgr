package com.ywwxhz.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;

import com.ywwxhz.model.AppInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;

public class FileUtil {

    private static final FilenameFilter APK_FILTER = new FilenameFilter() {

        @Override
        public boolean accept(File dir, String filename) {
            return filename.endsWith(".apk");
        }
    };

    public static File getBackUpDir(Context context) throws Exception {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            File path = new File(Environment.getExternalStorageDirectory()
                    .getAbsolutePath()
                    + "/"
                    + Util.getString(context, "path", "App"));
            if (!path.exists()) {
                path.mkdirs();
            }
            return path;
        }
        throw new Exception("can't read external storage");
    }

    public static long getFolderSize(Context context) throws Exception {
        long size = 0;
        File[] fileList = getBackUpDir(context).listFiles(APK_FILTER);
        for (File file : fileList) {
            size += file.length();
        }
        return size;
    }

    public static HashMap<String, AppInfo> getBackupAppMap(Context context)
            throws Exception {
        HashMap<String, AppInfo> apps = new HashMap<String, AppInfo>();
        File[] fileList = getBackUpDir(context).listFiles(APK_FILTER);
        for (File file : fileList) {
            AppInfo info = apkInfo(file.getAbsolutePath(), context);
            if (info != null) {
                apps.put(info.getPkgName(), info);
            }
        }

        return apps;
    }

    public static void cleanAPK(Context context) throws Exception {
        File[] fileList = getBackUpDir(context).listFiles(APK_FILTER);
        for (File file : fileList) {
            file.delete();
        }
    }

    /**
     * 复制文件(以超快的速度复制文件)
     *
     * @param srcFile     源文件File
     * @param destDir     目标目录File
     * @param newFileName 新文件名
     * @return 实际复制的字节数，如果文件、目录不存在、文件为null或者发生IO异常，返回-1
     */
    public static long copyFile(String srcFile, File destDir, String newFileName) {
        long copySizes = 0;
        if (!destDir.exists()) {
            destDir.mkdirs();
        }
        if (newFileName == null) {
            System.out.println("文件名为null");
            return -1;
        }

        FileInputStream fsin = null;
        FileOutputStream fsout = null;
        try {
            fsin = new FileInputStream(srcFile);
            fsout = new FileOutputStream(new File(destDir, newFileName));
            FileChannel fcin = fsin.getChannel();
            FileChannel fcout = fsout.getChannel();
            long size = fcin.size();
            fcin.transferTo(0, fcin.size(), fcout);
            fcin.close();
            fcout.close();
            copySizes = size;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            copySizes = -1;
        } catch (IOException e) {
            e.printStackTrace();
            copySizes = -1;
        } finally {
            if (fsin != null)
                try {
                    fsin.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    copySizes = -1;
                }
            if (fsout != null)
                try {
                    fsout.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    copySizes = -1;
                }
        }
        return copySizes;
    }

    private static AppInfo apkInfo(String absPath, Context context) {
        AppInfo info = null;
        PackageManager pm = context.getPackageManager();
        PackageInfo pkgInfo = pm.getPackageArchiveInfo(absPath,
                PackageManager.GET_ACTIVITIES);
        if (pkgInfo != null) {
            info = new AppInfo();
            ApplicationInfo appInfo = pkgInfo.applicationInfo;
            /* 必须加这两句，不然下面icon获取是default icon而不是应用包的icon */
            appInfo.sourceDir = absPath;
            appInfo.publicSourceDir = absPath;
            info.setLabel(pm.getApplicationLabel(appInfo).toString());
            info.setPkgName(appInfo.packageName);
            info.setVersionCode(pkgInfo.versionCode);
            info.setVersion(pkgInfo.versionName);
            info.setAppIcon(pm.getApplicationIcon(appInfo));
            info.setSourceDir(absPath);
        }
        return info;
    }

    public static boolean isExist(Context context, String pkgName,
                                  int versionCode) {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            try {
                File file = new File(getBackUpDir(context).getAbsolutePath() + "/"
                        + pkgName + "_" + versionCode + ".apk");
                return file.exists();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }
}
