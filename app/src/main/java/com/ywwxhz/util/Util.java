package com.ywwxhz.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.WindowManager;

public class Util {
    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    @TargetApi(19)
    public static void setTranslucentStatus(Activity activity) {
        // 透明状态栏
        activity.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // 透明导航栏
        activity.getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
    }

    private static ForegroundColorSpan makeForegroundColorSpan(int r, int g,
                                                               int b) {
        return new ForegroundColorSpan(Color.rgb(r, g, b));
    }

    public static SpannableString setTextColor(String text, int r, int g, int b) {
        SpannableString span = new SpannableString(text);
        span.setSpan(makeForegroundColorSpan(r, g, b), 0, text.length(), 0);
        return span;
    }

    public static String getString(Context context, String key, String def) {
        SharedPreferences appPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        return appPrefs.getString(key, def);
    }
}
