package com.ywwxhz.app;

import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.MenuItem;
import android.view.View;

import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.ywwxhz.PkgMgr.R;
import com.ywwxhz.util.Util;

public class SettingsActivity extends PreferenceActivity {

    private SystemBarTintManager tintManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getListView().setClipToPadding(false);
        addPreferencesFromResource(R.xml.pref_package_general);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Util.setTranslucentStatus(this);
            tintManager = new SystemBarTintManager(this);
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setStatusBarTintResource(R.color.package_statusbar_bg);
            SystemBarTintManager.SystemBarConfig config = tintManager.getConfig();
            View rootView = getListView();
            rootView.setPadding(rootView.getPaddingLeft(), config.getPixelInsetTop(true)
                    + rootView.getPaddingTop(), config.getPixelInsetRight()
                    + rootView.getPaddingRight(), rootView.getPaddingBottom()
                    + tintManager.getConfig().getPixelInsetBottom());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            SystemBarTintManager.SystemBarConfig config = tintManager.getConfig();
            View rootView = getListView();
            rootView.setPadding(rootView.getPaddingLeft(), config.getPixelInsetTop(true)
                    , config.getPixelInsetRight() + rootView.getPaddingRight()
                    , tintManager.getConfig().getPixelInsetBottom());
        }
    }
}
