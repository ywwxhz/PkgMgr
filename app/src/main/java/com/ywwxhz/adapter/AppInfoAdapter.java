package com.ywwxhz.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.ywwxhz.PkgMgr.R;
import com.ywwxhz.model.AppInfo;
import com.ywwxhz.model.Category;
import com.ywwxhz.util.Util;

//自定义适配器类，提供给listView的自定义view
public class AppInfoAdapter extends BaseAdapter {

    LayoutInflater infater = null;
    Category category;

    Context context;

    public AppInfoAdapter(Context context, Category category) {
        infater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.category = category;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        ViewHolder holder = null;
        if (convertView == null || convertView.getTag() == null) {
            view = infater.inflate(R.layout.package_list_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) convertView.getTag();
        }
        AppInfo appInfo = category.getAppInfo(position);
        view.setActivated(appInfo.isSelect());
        holder.appIcon.setImageDrawable(appInfo.getAppIcon());
        holder.tvVersion.setText(appInfo.getVersion() + " ("
                + appInfo.getVersionCode() + ")");
        holder.isSelect.setChecked(appInfo.isSelect());
        if (appInfo.isBackup()) {
            holder.hasbackup.setVisibility(View.VISIBLE);
        } else {
            holder.hasbackup.setVisibility(View.INVISIBLE);
        }
        switch (appInfo.getType()) {
            case 0:
                holder.tvAppLabel.setText(Util.setTextColor(appInfo.getLabel(), 67,
                        191, 207));
                break;
            case 1:
                holder.tvAppLabel.setText(Util.setTextColor(appInfo.getLabel(),
                        104, 201, 104));
                holder.hasbackup.setVisibility(View.INVISIBLE);
                break;
            default:
                holder.tvAppLabel.setText(appInfo.getLabel());
                break;
        }
        return view;
    }

    @Override
    public int getCount() {
        return category.getSize();
    }

    @Override
    public Object getItem(int position) {
        return category.getAppInfo(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    class ViewHolder {
        ImageView appIcon;
        TextView tvAppLabel;
        TextView tvVersion;
        View hasbackup;
        CheckBox isSelect;

        public ViewHolder(View view) {
            this.appIcon = (ImageView) view.findViewById(R.id.imgApp);
            this.tvAppLabel = (TextView) view.findViewById(R.id.tvAppLabel);
            this.tvVersion = (TextView) view.findViewById(R.id.tvVersion);
            this.hasbackup = view.findViewById(R.id.hasbackup);
            this.isSelect = (CheckBox) view.findViewById(R.id.select);
        }
    }
}